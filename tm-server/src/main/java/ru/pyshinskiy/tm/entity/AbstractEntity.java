package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    private static final long SerialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();
}
