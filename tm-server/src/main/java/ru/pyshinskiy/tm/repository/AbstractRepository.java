package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public abstract class AbstractRepository {


    protected EntityManager em;

    public AbstractRepository() {
    }

    @NotNull
    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.em = entityManager;
    }
}
