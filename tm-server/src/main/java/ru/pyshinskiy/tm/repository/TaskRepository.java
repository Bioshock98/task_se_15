package ru.pyshinskiy.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull List<Task> findAllByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId", Task.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Task> findAll() {
        return em.createQuery("SELECT t FROM Task t", Task.class).getResultList();
    }

    @Override
    public @Nullable Task findOne(@NotNull final String id) {
        return em.find(Task.class, id);
    }

    @Override
    public void persist(@NotNull final Task task) {
        em.persist(task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        em.merge(task);
    }

    @Override
    public void remove(@NotNull final Task task) {
        em.remove(task);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Task ").executeUpdate();
    }

    @Override
    @Nullable
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT t FROM Task t WHERE t.id = :id and t.user.id = :userId", Task.class).setParameter("id", id).setParameter("userId", userId).getSingleResult();
    }

    @Override
    public @NotNull List<Task> findByName(@NotNull final String userId, @NotNull final String name) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId AND t.name = :name", Task.class).setParameter("userId", userId).setParameter("name", name).getResultList();
    }

    @Override
    public @NotNull List<Task> findByDescription(@NotNull final String userId, @NotNull final String description) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId AND t.description = :description", Task.class).setParameter("userId", userId).setParameter("description", description).getResultList();
    }

    @Override
    public void removeByUserId(@NotNull final String userId, @NotNull final String id) {
        em.createQuery("DELETE FROM Task t WHERE t.id = :id AND t.user.id = :userId").setParameter("id", id).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        em.createQuery("DELETE FROM Task t WHERE t.user.id = :userId").setParameter("userId", userId).executeUpdate();
    }

    @Override
    public @NotNull List<Task> sortByCreateTime(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId ORDER BY t.createTime", Task.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Task> sortByStartDate(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId ORDER BY t.startDate", Task.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Task> sortByFinishDate(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId ORDER BY t.finishDate", Task.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Task> sortByStatus(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id= :userId ORDER BY FIELD(t.status, 'PLANNED', 'IN_PROGRESS', 'DONE')", Task.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId and t.project.id = :projectId", Task.class).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }
}
