package ru.pyshinskiy.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull List<Project> findAllByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId", Project.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Project> findAll() {
        return em.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @Override
    public @Nullable Project findOne(@NotNull final String id) {
        return em.find(Project.class, id);
    }

    @Override
    public void persist(@NotNull final Project project) {
        em.persist(project);
    }

    @Override
    public void merge(@NotNull final Project project) {
        em.merge(project);
    }

    @Override
    public void remove(@NotNull final Project project) {
        em.remove(project);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Project ").executeUpdate();
    }

    @Override
    @Nullable
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT p FROM Project p WHERE p.id = :id and p.user.id = :userId", Project.class).setParameter("id", id).setParameter("userId", userId).getSingleResult();
    }

    @Override
    public @NotNull List<Project> findByName(@NotNull final String userId, @NotNull final String name) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId AND p.name = :name", Project.class).setParameter("userId", userId).setParameter("name", name).getResultList();
    }

    @Override
    public @NotNull List<Project> findByDescription(@NotNull final String userId, @NotNull final String description) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId AND p.description = :description", Project.class).setParameter("userId", userId).setParameter("description", description).getResultList();
    }

    @Override
    public void removeByUserId(@NotNull final String userId, @NotNull final String id) {
        em.createQuery("DELETE FROM Project p WHERE p.id = :id AND p.user.id = :userId").setParameter("id", id).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        for(@NotNull Project project : findAll()) {
            if(userId.equals(project.getUser().getId())) em.remove(project);
        }
    }

    @Override
    public @NotNull List<Project> sortByCreateTime(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY p.createTime", Project.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Project> sortByStartDate(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY p.startDate", Project.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Project> sortByFinishDate(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY p.finishDate", Project.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public @NotNull List<Project> sortByStatus(@NotNull final String userId, final int direction) {
        return em.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY FIELD(p.status, 'PLANNED', 'IN_PROGRESS', 'DONE')", Project.class).setParameter("userId", userId).getResultList();
    }
}
