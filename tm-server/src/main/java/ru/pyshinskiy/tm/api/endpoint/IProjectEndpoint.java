package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @Nullable
    ProjectDTO findOneProject(@Nullable final SessionDTO sessionDTO, @NotNull final String id) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> findAllProjectsByUserId(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @Nullable
    ProjectDTO findOneProjectByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    void persistProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) throws Exception;

    @WebMethod
    void mergeProject(@Nullable final SessionDTO sessionDTO, @NotNull final ProjectDTO projectDTO) throws Exception;

    @WebMethod
    void removeProject(@Nullable final SessionDTO sessionDTO, @NotNull final String id) throws Exception;

    @WebMethod
    void removeProjectByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void removeAllProjectsByUserId(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @Nullable
    List<ProjectDTO> findProjectByName(@Nullable final SessionDTO sessionDTO, @Nullable final String name) throws Exception;

    @WebMethod
    @Nullable
    List<ProjectDTO> findProjectByDescription(@Nullable final SessionDTO sessionDTO, @Nullable final String description) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> sortProjectsByCreateTime(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> sortProjectsByStartDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> sortProjectsByFinishDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> sortProjectsByStatus(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception;
}
