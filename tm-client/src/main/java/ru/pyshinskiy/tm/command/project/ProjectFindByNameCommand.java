package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectFindByNameCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String command() {
        return "project_find_by_name";
    }

    @Override
    @NotNull
    public String description() {
        return "find project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY NAME]");
        System.out.println("ENTER PROJECT NAME");
        @NotNull final String name = terminalService.nextLine();
        printProjects(projectEndpoint.findProjectByName(sessionService.getSessionDTO(), name));
        System.out.println("[OK]");
    }
}
