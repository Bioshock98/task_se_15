package ru.pyshinskiy.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

public final class HelpCommand extends AbstractCommand {

    @Inject @Any
    private Instance<AbstractCommand> commands;

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "help";
    }

    @Override
    @NotNull
    public String description() {
        return "list available commands";
    }

    @Override
    public void execute() {
        for(@NotNull final AbstractCommand command : commands) {
            if(command.isAllowed()) {
                System.out.println(command.command() + ": " +
                        command.description());
            }
        }
        System.out.println("[OK]");
    }
}
