package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class ProjectClearAdminCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all users projects";
    }

    @Override
    public void execute() throws Exception {
        projectEndpoint.removeAllProjects(sessionService.getSessionDTO());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
