package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskRemoveCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(sessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        taskEndpoint.removeTaskByUserId(sessionService.getSessionDTO(), taskId);
        System.out.println("[TASK REMOVED]");
    }
}
