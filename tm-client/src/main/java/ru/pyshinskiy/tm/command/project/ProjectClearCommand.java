package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class ProjectClearCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String command() {
        return "project_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        projectEndpoint.removeAllProjectsByUserId(sessionService.getSessionDTO());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
