package ru.pyshinskiy.tm.util.entity;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;

import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateToString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toDate;

public final class EntityUtil {

    public static void printProjects(@NotNull final List<ProjectDTO> projects) {
        for (int i = 0; i < projects.size(); i++) {
            @NotNull final ProjectDTO projectDTO = projects.get(i);
            System.out.println((i + 1) + "." + " " + projectDTO.getName());
        }
    }

    public static void printProject(@NotNull final ProjectDTO projectDTO) {
        if(projectDTO.getStartDate() == null) {
            System.out.println("project start date is null");
            return;
        }
        if(projectDTO.getFinishDate() == null) {
            System.out.println("project end date is null");
            return;
        }
        @NotNull final StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(projectDTO.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(projectDTO.getDescription());
        formatedProject.append("\nproject status: ");
        formatedProject.append(projectDTO.getStatus());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(toDate(projectDTO.getStartDate())));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(toDate(projectDTO.getStartDate())));
        System.out.println(formatedProject);
    }

    public static void printTasks(@NotNull final List<TaskDTO> tasksDTO) {
        for (int i = 0; i < tasksDTO.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasksDTO.get(i).getName());
        }
    }

    public static void printTask(@NotNull final TaskDTO taskDTO) {
        if(taskDTO.getStartDate() == null) {
            System.out.println("project start date is null");
            return;
        }
        if(taskDTO.getFinishDate() == null) {
            System.out.println("project end date is null");
            return;
        }
        @NotNull final StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(taskDTO.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(taskDTO.getDescription());
        formatedTask.append("\ntask status: ");
        formatedTask.append(taskDTO.getStatus());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(toDate(taskDTO.getStartDate())));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(toDate(taskDTO.getFinishDate())));
        System.out.println(formatedTask);
    }

    public static void printUsers(@NotNull final List<UserDTO> usersDTO) {
        for(int i = 0; i < usersDTO.size(); i++) {
            System.out.println((i + 1) + ". username: " + usersDTO.get(i).getLogin());
            System.out.println("role: " + usersDTO.get(i).getRole());
        }
    }

    public static void printUser(@NotNull final UserDTO userDTO) {
        System.out.println("username: " + userDTO.getLogin());
        System.out.println("role: " + userDTO.getRole());
    }
}
